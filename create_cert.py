certbot certonly --standalone -d $1 --config-dir '/home/docka/etc/letsencrypt' --work-dir '/home/docka/var/lib/letsencrypt' --logs-dir '/home/docka/var/log/letsencrypt'
chown -R docka. /home/docka/etc
chown -R docka. /home/docka/var
