# Django CMS Docker/Podman Image with responsive grids

This repository provides a docker-compose build system (gitlab ci) for a Django CMS docker image.
The image is used in docker and podman (podman-compose) deployment processes.


## Django CMS app "grids"

The django project includes the Django CMS app "grids".
The grids app provides responsive content plugins for Django CMS.
It is based on a foundation 6 integration.

Grids provides the following plugins:

- Initiale
- Grid (for columns/cells)
- Double Column
- Triple Column
- Video
- Timeline
- Checkmark list
- Testimonial
- Fullwidth Testimonial
- Gallery
- Callout
- Map
- Overlay (Text above Image)
- Fullwidth Box
- Fixed Height Gallery
- Contact form


## Future developments

The grids app - currently included in this repository - will be uploaded as a pypi package (e.g. djangocms-foundation-grids)
and installed as a pip requirement in future releases of this repository.



# Installation for production


## Copy docker compose default file
    cp docker-compose.yml_default docker-compose.yml


## Copy default dotenv file and load environment variables
    cp django.env_default web/.env
    cd web
    source setenv.sh


## Copy default files to project specific files to prevent overwriting of files:

    cp project/settings/local.py_default project/settings/local.py
    cp project/settings/prod_first.py_default project/settings/prod_first.py
    cp project/static/project/scss/app_first.scss_default  project/static/project/scss/app_first.scss
    cp project/static/project/scss/project_settings.scss_default  project/static/project/scss/project_settings.scss
    cp project/templates/fullwidth_first.html_default  project/templates/fullwidth_first.html


## Start docker containers
    docker-compose up -d


## Inside web container run the migration, collectstatic and createsuperuser commands
    docker exec -ti djangocms_web_1 /bin/sh
    python3 manage.py migrate
    python3 manage.py collectstatic
    python3 manage.py createsuperuser



# Installation for development


## Install required system packages
    apt-get update
    apt install -y python3-pip gdal-bin python3-gdal


## Install virtual environment and python requirements to run tests outside containers
    pip install virtualenv --upgrade pip
    virtualenv venv
    source venv/bin/activate
    cd web
    source setenv.sh
    pip install -r requirements.txt
    python3 manage.py migrate
    python3 manage.py check
    python manage.py test grids
