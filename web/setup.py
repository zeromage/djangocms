#!/usr/bin/env python
from setuptools import setup
setup(
    name='grids',
    packages=['grids'],
    version='0.0.1',
    description='responsive plugins for djangocms',
    author='Mona Hagedorn',
    author_email='zeromage@neo.codes',
    keywords=['django', 'cms', 'foundation', 'grids', 'responsive'],
    # install_requires=open("requirements/base.in").readlines(),
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Topic :: Internet',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Internet :: WWW/HTTP :: WSGI',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)

