# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from cms.models import CMSPlugin

from djangocms_text_ckeditor.models import AbstractText
from djangocms_text_ckeditor.fields import HTMLField

from djangocms_picture.models import Picture


GRID_CONFIG = {'COLUMNS': 24, 'TOTAL_WIDTH': 960, 'GUTTER': 20}
GRID_CONFIG.update(getattr(settings, 'DJANGOCMS_GRID_CONFIG', {}))

DJANGOCMS_GRID_CHOICES = [
    ('%s' % i, 'grid-%s' % i) for i in reversed(range(1, GRID_CONFIG['COLUMNS'] + 1))
]
DJANGOCMS_GRID_CHOICES += [
    ('', 'No size'),
]

if hasattr(settings, 'GRID_STYLES') and len(settings.GRID_STYLES) > 0:
    STYLES = settings.GRID_STYLES
else:
    STYLES = []

GRID_TYPES = [
    ('grid-x', 'Horizontales Grid'),
    ('grid-y', 'Vertikales Grid'),
]


class GalleryImage(Picture):
    """The class for a GalleryImage"""
    list_order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['list_order']


# @python_2_unicode_compatible
class Grid(CMSPlugin):
    """The model for a grid"""
    grid_type = models.CharField(
        _('Grid Type'), default='grid-x', choices=GRID_TYPES, max_length=200)
    margin_x = models.BooleanField('Margin x', default=True)
    margin_y = models.BooleanField('Margin y', default=False)
    padding_x = models.BooleanField('Padding x', default=False)
    padding_y = models.BooleanField('Padding y', default=False)
    content_element = models.BooleanField('Content element', default=True)
    frame = models.BooleanField('Frame', default=False, help_text=_('Create 100% height for vertical grids.'))
    small_frame = models.BooleanField('Small Grid Frame', default=False,
                                      help_text=_('Create 100% height for vertical grids for small resolution.'))
    medium_frame = models.BooleanField('Medium Grid Frame', default=False,
                                       help_text=_('Create 100% height for vertical grids for medium resolution.'))
    large_frame = models.BooleanField('Large Grid Frame', default=False,
                                      help_text=_('Create 100% height for vertical grids for large resolution.'))
    padding_gutters = models.BooleanField('Padding gutters', default=False)
    grid_container = models.BooleanField('Grid container', default=False)
    fluid = models.BooleanField('Fluid', default=False)
    light = models.BooleanField('Light', default=False, help_text=_('Light text color.'))
    style = models.CharField(
        _('style'), choices=STYLES, max_length=200, null=True, blank=True)
    custom_classes = models.CharField(
        _('custom classes'), max_length=200, null=True, blank=True)
    image = models.ForeignKey(GalleryImage, on_delete=models.CASCADE, null=True, blank=True)
    translatable_content_excluded_fields = ['custom_classes']

    def __str__(self):
        return _("%s columns") % self.cmsplugin_set.all().count()


# @python_2_unicode_compatible
class GridColumn(CMSPlugin):
    """The model for a column

    Columns will be deprecated.
    They are used in older projects to allow
    smooth migration of contents in columns
    """
    size = models.CharField(
        _('size'), choices=DJANGOCMS_GRID_CHOICES, default='1', max_length=50)
    custom_classes = models.CharField(
        _('custom classes'), max_length=200, blank=True)
    translatable_content_excluded_fields = ['custom_classes']

    def __str__(self):
        return "%s" % self.get_size_display()


# @python_2_unicode_compatible
class GridCell(CMSPlugin):
    """The model for a cell

    Cells allow more settings compared to columns.
    """
    small_size = models.CharField(_('Small size'), choices=DJANGOCMS_GRID_CHOICES,
                                  default='1', max_length=50, null=True, blank=True)
    medium_size = models.CharField(_('Medium size'), choices=DJANGOCMS_GRID_CHOICES,
                                   default='1', max_length=50, null=True, blank=True)
    large_size = models.CharField(_('Large size'), choices=DJANGOCMS_GRID_CHOICES,
                                  default='1', max_length=50, null=True, blank=True)
    shrink = models.BooleanField('Shrink', default=False, help_text=_('Shrink to content size.'))
    auto = models.BooleanField('Auto', default=False, help_text=_('Take up the remaining space.'))
    small_auto = models.BooleanField('Small auto', default=False,
                                     help_text=_('Take up the remaining space for small resolution.'))
    medium_auto = models.BooleanField('Medium auto', default=False,
                                      help_text=_('Take up the remaining space for medium resolution.'))
    large_auto = models.BooleanField('Large auto', default=False,
                                     help_text=_('Take up the remaining space for large resolution.'))
    medium_cell_block_container = models.BooleanField('Medium cell block container', default=False)
    medium_cell_block_x = models.BooleanField('Medium cell block x', default=False)
    medium_cell_block_y = models.BooleanField('Medium cell block y', default=False)
    custom_classes = models.CharField(
        _('custom classes'), max_length=200, blank=True)
    translatable_content_excluded_fields = ['custom_classes']

    def __str__(self):
        return "%s" % self.get_small_size_display()


class InitialeText(AbstractText):
    """The model for a initiale text"""
    title = models.CharField(null=True, blank=True, max_length=255)


class Video(CMSPlugin):
    """The model for video"""
    title = models.CharField(max_length=70, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    url = models.CharField(max_length=255)

    def __str__(self):
        return self.title or self.url


class Testimonial(CMSPlugin):
    """The model for testimonial"""
    name = models.CharField(max_length=70, blank=True, null=True)
    bodytext = HTMLField(blank=True, null=True)
    image = models.ForeignKey(GalleryImage, null=True, blank=True, on_delete=models.CASCADE)
    my_order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['my_order']

    def __str__(self):
        return self.name


class TestimonialPluginModel(CMSPlugin):
    """The model for a set of testimonial plugin

    The testimonial plugin references multiple testimonials"""
    testimonials = models.ManyToManyField(Testimonial, related_name='plugins', through='TestimonialList')

    def copy_relations(self, oldinstance):
        TestimonialList.objects.filter(plugin=oldinstance)
        for item in TestimonialList.objects.filter(plugin=oldinstance):
            new_item = TestimonialList.objects.create(plugin=self, testimonial=item.testimonial,
                                                      list_order=item.list_order)
            new_item.save()


class FullWidthTestimonialPluginModel(CMSPlugin):
    """The model for fullwidth testimonial"""
    name = models.CharField(max_length=70, blank=True, null=True)
    bodytext = HTMLField(blank=True, null=True)
    claim = models.CharField(max_length=255, blank=True, null=True)


class TimelineNode(models.Model):
    """The model for a timeline node"""
    title = models.CharField(max_length=70, blank=True, null=True)
    bodytext = models.TextField(blank=True, null=True)
    list_order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['list_order']

    def __str__(self):
        return self.title


class CheckmarkNode(models.Model):
    """The model for a checkmark node"""
    bodytext = models.TextField(blank=True, null=True)
    list_order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['list_order']

    def __str__(self):
        return self.bodytext


class TimelinePluginModel(CMSPlugin):
    """The model for a timeline plugin

    The timeline plugin references multiple timeline nodes."""
    timeline_nodes = models.ManyToManyField(TimelineNode, related_name='plugins', through='TimelineNodeList')

    def copy_relations(self, oldinstance):
        TimelineNodeList.objects.filter(plugin=oldinstance)
        for item in TimelineNodeList.objects.filter(plugin=oldinstance):
            new_item = TimelineNodeList.objects.create(plugin=self, timelinenode=item.timelinenode,
                                                       list_order=item.list_order)
            new_item.save()


class CheckmarkListPluginModel(CMSPlugin):
    """The model for a checkmark list

    The checkmark list references multiple checkmark nodes."""
    list_nodes = models.ManyToManyField(CheckmarkNode, related_name='plugins', through='CheckmarkNodeList')

    def copy_relations(self, oldinstance):
        CheckmarkNodeList.objects.filter(plugin=oldinstance)
        for item in CheckmarkNodeList.objects.filter(plugin=oldinstance):
            new_item = CheckmarkNodeList.objects.create(plugin=self, checkmarknode=item.checkmarknode,
                                                        list_order=item.list_order)
            new_item.save()


class GalleryPluginModel(CMSPlugin):
    """The model for a gallery

    The gallery references multiple gallery images."""
    images = models.ManyToManyField(GalleryImage, related_name='plugins')

    def copy_relations(self, oldinstance):
        for image in oldinstance.images.all():
            self.images.add(image)
        self.save()


class FixedPluginModel(CMSPlugin):
    """The model for a fixed height gallery

    The gallery references multiple gallery images."""
    images = models.ManyToManyField(GalleryImage, related_name='fixed_plugins')

    def copy_relations(self, oldinstance):
        for image in oldinstance.images.all():
            self.images.add(image)
        self.save()


class FixedWidthTable(CMSPlugin):
    """The model for a fixed height gallery

    The gallery references multiple gallery images."""
    images = models.ManyToManyField(GalleryImage, through='GalleryImageList')

    def copy_relations(self, oldinstance):
        self.images.clear()
        for image in oldinstance.images.all():
            img = GalleryImageList()
            img.image = image
            img.plugin = self
            img.save()


class TestimonialList(models.Model):
    """The model for a testimonial list"""
    testimonial = models.ForeignKey(Testimonial, on_delete=models.CASCADE)
    plugin = models.ForeignKey(TestimonialPluginModel, on_delete=models.CASCADE)
    list_order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['list_order']


class TimelineNodeList(models.Model):
    """The model for a timelinenode list"""
    timelinenode = models.ForeignKey(TimelineNode, on_delete=models.CASCADE)
    plugin = models.ForeignKey(TimelinePluginModel, on_delete=models.CASCADE)
    list_order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['list_order']


class CheckmarkNodeList(models.Model):
    """The model for a checkmarknode list"""
    checkmarknode = models.ForeignKey(CheckmarkNode, on_delete=models.CASCADE)
    plugin = models.ForeignKey(CheckmarkListPluginModel, on_delete=models.CASCADE)
    list_order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['list_order']


class GalleryImageList(models.Model):
    """The model for a gallery image list

    The gallery image list references multiple gallery image nodes."""
    image = models.ForeignKey(GalleryImage, on_delete=models.CASCADE)
    plugin = models.ForeignKey(FixedWidthTable, on_delete=models.CASCADE)
    list_order = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta(object):
        ordering = ['list_order']


class OverlayPluginModel(CMSPlugin):
    """The model for an overlay"""
    image = models.ForeignKey(GalleryImage, on_delete=models.CASCADE)


class ContactFormEntry(models.Model):
    """The model for a contact form entry"""
    first_name = models.CharField(_('Vorname'), max_length=255)
    last_name = models.CharField(_('Nachname'), max_length=255)
    company = models.CharField(_('Unternehmen / Organisation'), max_length=255, blank=True, null=True)
    email = models.CharField(_('E-Mail'), max_length=255)
    phone = models.CharField(_('Telefon'), max_length=255)
    message = models.TextField(_('Nachricht'), )

    def __str__(self):
        return '{} ({} {})'.format(self.email, self.first_name, self.last_name)


admin.site.register(Testimonial)
admin.site.register(TimelineNode)
admin.site.register(CheckmarkNode)
admin.site.register(TestimonialPluginModel)
admin.site.register(TimelinePluginModel)
admin.site.register(CheckmarkListPluginModel)
admin.site.register(FullWidthTestimonialPluginModel)
admin.site.register(Picture)
admin.site.register(GalleryImage)
admin.site.register(GalleryPluginModel)
admin.site.register(FixedPluginModel)
admin.site.register(FixedWidthTable)
admin.site.register(ContactFormEntry)
admin.site.register(OverlayPluginModel)
