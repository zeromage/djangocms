# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings
from django.contrib import admin
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

from cms.models import CMSPlugin
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

# from cmsplugin_form_handler.cms_plugins import FormPluginBase

from easy_thumbnails.files import get_thumbnailer

from grids.forms import GridPluginForm, GridDoublePluginForm, ContactForm

from adminsortable2.admin import SortableInlineAdminMixin

from djangocms_text_ckeditor.cms_plugins import TextPlugin
from .models import (
    Grid, GridColumn, GridCell, GRID_CONFIG,  # , FormSettings
    InitialeText, Video,
    TestimonialPluginModel,
    FullWidthTestimonialPluginModel, TimelinePluginModel, CheckmarkListPluginModel,
    GalleryPluginModel, FixedWidthTable,
    OverlayPluginModel
)


class InitialeTextPlugin(TextPlugin):
    """Plugin for initiales

    The plugin displays an initiale as the first char of a given title.
    """
    name = _(u"Initiale Text Plugin")
    model = InitialeText
    render_template = "grids/initialetext.html"

    def render(self, context, instance, placeholder):
        context.update({
            'char': instance.title[0],
            'title': instance.title,
        })
        # Other custom render code you may have
        return super(InitialeTextPlugin, self).render(context, instance, placeholder)


plugin_pool.register_plugin(InitialeTextPlugin)


try:
    xrange
except NameError:
    xrange = range


@plugin_pool.register_plugin
class GridPlugin(CMSPluginBase):
    """Plugin for grids

    The plugin displays a grid with one or multiple cells.
    It offers settings to customize the appearance of the grid.
    """
    model = Grid
    name = _('Mehrspalten Plugin')
    module = _('Multi Columns')
    render_template = 'grids/grid.html'
    allow_children = True
    child_classes = ['GridCellPlugin', 'GridColumnPlugin']
    form = GridPluginForm

    def render(self, context, instance, placeholder):
        context.update({
            'grid': instance,
            'placeholder': placeholder,
            'GRID_SIZE': GRID_CONFIG['COLUMNS'],
        })
        return context

    def save_model(self, request, obj, form, change):
        response = super(GridPlugin, self).save_model(request, obj, form,
                                                      change)
        for x in xrange(int(form.cleaned_data['create'])):
            if CMSPlugin.objects.filter(parent=obj).count() < int(form.cleaned_data['create']):
                col = GridCell(
                    parent=obj, placeholder=obj.placeholder, language=obj.language,
                    small_size=form.cleaned_data['create_size'],
                    medium_size=form.cleaned_data['create_size'],
                    large_size=form.cleaned_data['create_size'],
                    position=CMSPlugin.objects.filter(parent=obj).count(),
                    plugin_type=GridCellPlugin.__name__
                )
                col.save()
        return response


@plugin_pool.register_plugin
class GridColumnPlugin(CMSPluginBase):
    """Plugin for grid columns

    The plugin displays a grid column.
    This plugin is a legacy plugin, which will be deprecated, but allows
    smooth migration of older projects which used columns instead of cells.
    """
    model = GridColumn
    name = _('Grid column')
    module = _('Multi Columns')
    render_template = 'grids/column.html'
    allow_children = True

    def render(self, context, instance, placeholder):
        context.update({
            'column': instance,
            'placeholder': placeholder,
            'width': (GRID_CONFIG['TOTAL_WIDTH'] / GRID_CONFIG['COLUMNS'] *
                      int(instance.size) - GRID_CONFIG['GUTTER'])
        })
        return context


@plugin_pool.register_plugin
class GridCellPlugin(CMSPluginBase):
    """Plugin for grid cells

    The plugin displays a grid cell.
    Compared to the legacy columns plugin, which will be deprecated and only
    allowed horizontal grids, it allows vertical grids.
    """
    model = GridCell
    name = _('Grid cell')
    module = _('Multi Columns')
    render_template = 'grids/cell.html'
    allow_children = True

    def render(self, context, instance, placeholder):
        context.update({
            'cell': instance,
            'placeholder': placeholder,
        })
        return context


@plugin_pool.register_plugin
class GridDoublePlugin(CMSPluginBase):
    """Plugin for grids with two cells

    The plugin displays a grid with two cells.
    The user can choose the size of the two cells.
    """
    model = Grid
    name = _('Mehrspalten Double Plugin')
    module = _('Multi Columns')
    render_template = 'grids/grid.html'
    allow_children = True
    child_classes = ['GridCellPlugin', 'GridColumnPlugin']
    form = GridDoublePluginForm

    def render(self, context, instance, placeholder):
        context.update({
            'grid': instance,
            'placeholder': placeholder,
            'GRID_SIZE': GRID_CONFIG['COLUMNS'],
        })
        return context

    def save_model(self, request, obj, form, change):
        response = super(GridDoublePlugin, self).save_model(request, obj, form,
                                                            change)
        if CMSPlugin.objects.filter(parent=obj).count() < 2:
            col = GridCell(
                parent=obj, placeholder=obj.placeholder, language=obj.language,
                small_size=6,
                medium_size=6,
                large_size=6,
                position=CMSPlugin.objects.filter(parent=obj).count(),
                plugin_type=GridCellPlugin.__name__
            )
            col.save()
        if CMSPlugin.objects.filter(parent=obj).count() < 2:
            col = GridCell(
                parent=obj, placeholder=obj.placeholder, language=obj.language,
                small_size=6,
                medium_size=6,
                large_size=6,
                position=CMSPlugin.objects.filter(parent=obj).count(),
                plugin_type=GridCellPlugin.__name__
            )
            col.save()
        return response


@plugin_pool.register_plugin
class GridTriplePlugin(CMSPluginBase):
    """Plugin for grids with three cells

    The plugin displays a grid with three cells.
    The user can choose the size of the three cells.
    """
    name = _('Mehrspalten Triple Plugin')
    module = _('Multi Columns')
    render_template = 'grids/grid.html'
    allow_children = True
    child_classes = ['GridCellPlugin', 'GridColumnPlugin']

    def render(self, context, instance, placeholder):
        context.update({
            'grid': instance,
            'placeholder': placeholder,
            'GRID_SIZE': GRID_CONFIG['COLUMNS'],
        })
        return context

    def save_model(self, request, obj, form, change):
        response = super(GridTriplePlugin, self).save_model(request, obj, form,
                                                            change)
        if CMSPlugin.objects.filter(parent=obj).count() < 3:
            col = GridCell(
                parent=obj, placeholder=obj.placeholder, language=obj.language,
                small_size=4,
                medium_size=4,
                large_size=4,
                position=CMSPlugin.objects.filter(parent=obj).count(),
                plugin_type=GridCellPlugin.__name__
            )
            col.save()

        if CMSPlugin.objects.filter(parent=obj).count() < 3:
            col = GridCell(
                parent=obj, placeholder=obj.placeholder, language=obj.language,
                small_size=4,
                medium_size=4,
                large_size=4,
                position=CMSPlugin.objects.filter(parent=obj).count(),
                plugin_type=GridCellPlugin.__name__
            )
            col.save()

        if CMSPlugin.objects.filter(parent=obj).count() < 3:
            col = GridCell(
                parent=obj, placeholder=obj.placeholder, language=obj.language,
                small_size=4,
                medium_size=4,
                large_size=4,
                position=CMSPlugin.objects.filter(parent=obj).count(),
                plugin_type=GridCellPlugin.__name__
            )
            col.save()
        return response


@plugin_pool.register_plugin
class VideoPlugin(CMSPluginBase):
    """Plugin for videos

    The plugin displays a video.
    """
    model = Video
    name = _("Video Plugin")
    render_template = "video.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super(VideoPlugin, self).render(context, instance, placeholder)
        return context


class MembershipInlineAdmin(SortableInlineAdminMixin, admin.StackedInline):
    model = TestimonialPluginModel.testimonials.through
    # fk_name = 'testimonialpluginmodel_set'


class TimelineNodeInlineAdmin(SortableInlineAdminMixin, admin.StackedInline):
    """Inline admin for timeline plugins"""
    model = TimelinePluginModel.timeline_nodes.through


class CheckmarkNodeInlineAdmin(SortableInlineAdminMixin, admin.StackedInline):
    """Inline admin for checkmark list plugins"""
    model = CheckmarkListPluginModel.list_nodes.through


class PictureInlineAdmin(admin.StackedInline):
    """Inline admin gallery plugins"""
    model = GalleryPluginModel.images.through


class GalleryImageInlineAdmin(admin.StackedInline):
    """Inline admin gallery plugins"""
    model = GalleryPluginModel.images.through


class FixedImageInlineAdmin(SortableInlineAdminMixin, admin.StackedInline):
    """Inline admin fixed width tables"""
    # class FixedImageInlineAdmin(admin.StackedInline):
    model = FixedWidthTable.images.through


@plugin_pool.register_plugin
class TestimonialPlugin(CMSPluginBase):
    """Plugin for testimonials

    The plugin displays a testimonial.
    """
    model = TestimonialPluginModel
    name = _("Testimonial Plugin")
    render_template = "testimonial.html"
    cache = False
    inlines = (MembershipInlineAdmin,)
    exclude = ('testimonials',)

    def render(self, context, instance, placeholder):
        context = super(TestimonialPlugin, self).render(context, instance, placeholder)
        testimonials = instance.testimonials.all().order_by('testimoniallist__list_order')
        context.update({
            'testimonials': testimonials,
        })
        return context


@plugin_pool.register_plugin
class FullWidthTestimonialPlugin(CMSPluginBase):
    """Plugin for fullwidth testimonials

    The plugin displays a fullwidth testimonial.
    """
    model = FullWidthTestimonialPluginModel
    name = _("Fullwidth Testimonial Plugin")
    render_template = "full-width-testimonial.html"
    cache = False

    def render(self, context, instance, placeholder):
        context = super(FullWidthTestimonialPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class TimelinePlugin(CMSPluginBase):
    """Plugin for timelines

    The plugin displays a timeline.
    """
    model = TimelinePluginModel
    name = _("Timeline Plugin")
    render_template = "grids/timeline.html"
    cache = False
    inlines = (TimelineNodeInlineAdmin,)
    exclude = ('timeline_nodes',)

    def render(self, context, instance, placeholder):
        context = super(TimelinePlugin, self).render(context, instance, placeholder)
        timeline_nodes = instance.timeline_nodes.all().order_by('timelinenodelist__list_order')
        context.update({
            'nodes': timeline_nodes,
        })
        return context


@plugin_pool.register_plugin
class CheckmarkListPlugin(CMSPluginBase):
    """Plugin for checkmark lists

    The plugin displays a checkmark list.
    """
    model = CheckmarkListPluginModel
    name = _("Checkmark List Plugin")
    render_template = "grids/checkmarklist.html"
    cache = False
    inlines = (CheckmarkNodeInlineAdmin,)
    # exclude = ('timeline_nodes',)

    def render(self, context, instance, placeholder):
        context = super(CheckmarkListPlugin, self).render(context, instance, placeholder)
        nodes = instance.list_nodes.all().order_by('checkmarknodelist__list_order')
        context.update({
            'nodes': nodes,
        })
        return context


@plugin_pool.register_plugin
class GalleryPlugin(CMSPluginBase):
    """Plugin for galleries

    The plugin displays a gallery.
    """
    model = GalleryPluginModel
    name = _("Gallery Plugin")
    render_template = "grids/gallery.html"
    cache = False
    inlines = (GalleryImageInlineAdmin,)
    exclude = ('images',)

    def render(self, context, instance, placeholder):
        context = super(GalleryPlugin, self).render(context, instance, placeholder)
        images = instance.images.all()
        context.update({
            'images': images,
        })
        return context


@plugin_pool.register_plugin
class CalloutPlugin(CMSPluginBase):
    """Plugin for callouts

    The plugin displays a callout.
    """
    name = _('Callout')
    render_template = 'grids/callout.html'
    allow_children = True

    def render(self, context, instance, placeholder):
        context.update({
            'column': instance,
        })
        return context


@plugin_pool.register_plugin
class MapPlugin(CMSPluginBase):
    """Plugin for maps

    The plugin displays a map.
    """
    name = _('Map')
    render_template = 'grids/map.html'

    def render(self, context, instance, placeholder):
        location = settings.MAP_LOCATION
        context.update({
            'column': instance,
            'map_address': settings.MAP_ADDRESS,
            'map_location': '[{0},{1}]'.format(*location)
        })
        return context


@plugin_pool.register_plugin
class OverlayPlugin(CMSPluginBase):
    """Plugin for overlays

    The plugin display an overlay.
    An overlay is the combination of text content
    flowing over a background image.
    """
    model = OverlayPluginModel
    name = _('Overlay')
    render_template = 'grids/overlay.html'
    allow_children = True

    def render(self, context, instance, placeholder):
        context.update({
            'column': instance,
        })
        return context


@plugin_pool.register_plugin
class FullWidthBoxPlugin(CMSPluginBase):
    """Plugin for boxes

    The plugin displays a box streching to fullwidth.
    """
    name = _('Full Width Box')
    render_template = 'grids/full-width-box.html'
    allow_children = True

    def render(self, context, instance, placeholder):
        context.update({
            'column': instance,
        })
        return context


@plugin_pool.register_plugin
class FixedHeightPlugin(CMSPluginBase):
    """Plugin for fixed height galleries

    The plugin displays a table of images width a fixed height.
    """
    model = FixedWidthTable
    name = _('Fixed Height')
    render_template = 'grids/fixed_height.html'
    cache = False
    inlines = (FixedImageInlineAdmin,)
    exclude = ('images',)

    def render(self, context, instance, placeholder):
        context = super(FixedHeightPlugin, self).render(context, instance, placeholder)
        images = instance.images.all().order_by('galleryimagelist__list_order')
        fixed_height = 300
        max_width = 955
        rows = list()
        row_width = 0
        rows.append(list())
        row_num = 0
        for image in images:
            width = image.picture.width
            height = image.picture.height
            ratio = height / fixed_height
            calc_width = width / ratio
            options = {'size': (calc_width, fixed_height), 'crop': True}
            thumb_url = get_thumbnailer(image.picture).get_thumbnail(options).url
            row_width += calc_width
            if row_width <= max_width:
                rows[row_num].append([image, thumb_url])
            else:
                lag_without_new_item = max_width - (row_width - calc_width)
                overflow_with_new_item = row_width - max_width
                weighted_lag = lag_without_new_item / len(rows[row_num])
                weighted_overflow = overflow_with_new_item / (len(rows[row_num]) + 1)
                if lag_without_new_item < overflow_with_new_item:
                    # pump up existing and crop vertically
                    if len(rows[row_num]) == 1:
                        img = rows[row_num][0][0]
                        new_width = max_width
                        new_options = {'size': (new_width, fixed_height), 'crop': True}
                        new_thumb_url = get_thumbnailer(img.picture).get_thumbnail(new_options).url
                        rows[row_num][0][1] = new_thumb_url
                    else:
                        new_row_width = 0
                        for i, img in enumerate([item[0] for item in rows[row_num]][:-1]):
                            img_ratio = img.picture.height / fixed_height
                            img_calc_width = img.picture.width / img_ratio
                            weighted_lag = (img_calc_width / (row_width-calc_width)) * lag_without_new_item
                            new_width = int(img_calc_width + weighted_lag)
                            new_row_width += new_width
                            new_options = {'size': (new_width, fixed_height), 'crop': True}
                            new_thumb_url = get_thumbnailer(img.picture).get_thumbnail(new_options).url
                            rows[row_num][i][1] = new_thumb_url
                        img = rows[row_num][-1][0]
                        img_ratio = img.picture.height / fixed_height
                        img_calc_width = img.picture.width / img_ratio
                        weighted_lag = (img_calc_width / (row_width-calc_width)) * lag_without_new_item
                        new_width = max_width - new_row_width
                        new_options = {'size': (new_width, fixed_height), 'crop': True}
                        new_thumb_url = get_thumbnailer(img.picture).get_thumbnail(new_options).url
                        rows[row_num][-1][1] = new_thumb_url

                    rows.append(list())
                    row_num += 1
                    rows[row_num].append([image, thumb_url])
                    row_width = calc_width
                else:
                    # add new item and crop all horizontally
                    # overflow_ratio = overflow_with_new_item / max_width
                    new_row_width = 0
                    for i, img in enumerate([item[0] for item in rows[row_num]]):
                        img_ratio = img.picture.height / fixed_height
                        img_calc_width = img.picture.width / img_ratio
                        weighted_overflow = (img_calc_width / (row_width-calc_width)) * overflow_with_new_item
                        new_width = int(img_calc_width - weighted_overflow)
                        new_row_width += new_width
                        new_options = {'size': (new_width, fixed_height), 'crop': True}
                        new_thumb_url = get_thumbnailer(img.picture).get_thumbnail(new_options).url
                        rows[row_num][i][1] = new_thumb_url
                    img = image
                    img_ratio = img.picture.height / fixed_height
                    img_calc_width = img.picture.width / img_ratio
                    weighted_overflow = (img_calc_width / row_width) * overflow_with_new_item
                    new_width = img_calc_width - weighted_overflow
                    new_width = max_width - new_row_width
                    new_options = {'size': (new_width, fixed_height), 'crop': True}
                    new_thumb_url = get_thumbnailer(img.picture).get_thumbnail(new_options).url
                    rows[row_num].append([image, new_thumb_url])
                    rows.append(list())
                    row_num += 1
                    row_width = 0
        context.update({
            'images': images,
            'rows': rows
        })
        return context


@plugin_pool.register_plugin
class ContactFormPlugin(CMSPluginBase):
    """Plugin for contact forms

    The plugin displays a contact form.
    It will send the message the mail address
    defined in the settings.
    """
    name = _("Contact Form Plugin")
    render_template = "grids/contact_form.html"

    def render(self, context, instance, placeholder):
        context = super(ContactFormPlugin, self).render(context, instance, placeholder)
        request = context['request']
        form = ContactForm(request.POST) if request.method == 'POST' else ContactForm()
        is_valid = False

        if request.method == "POST" and form.is_valid():
            self.send(request, instance, form)
            is_valid = True

        context.update({
            'instance': instance,
            'form': form,
            'is_valid': is_valid
        })
        return context

    def send(self, request, instance, form):
        # Optionally do something with the rendered form here
        # This is what this method does by default:
        entry = form.save()
        # Do other stuff
        msg_plain = render_to_string('grids/email.txt', {'entry': entry})
        msg_html = render_to_string('grids/email.html', {'entry': entry})

        send_mail(
            'Kontaktformular',
            msg_plain,
            settings.EMAIL_HOST_USER,
            [settings.EMAIL_HOST_USER],
            html_message=msg_html,
        )
