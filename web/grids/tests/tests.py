from django.conf import settings
from django.test import TestCase

from grids.cms_plugins import (InitialeTextPlugin, VideoPlugin, FullWidthTestimonialPlugin,
                               CalloutPlugin, MapPlugin, OverlayPlugin)

from grids.tests.helpers import create_model_instance, get_render_context, render, create_gallery_image


class GridsTest(TestCase):

    def setUp(self):
        settings.MAP_ADDRESS = '<b>First Company</b><br>Some street<br>55555 City'
        settings.MAP_LOCATION = [50.7985410, 7.2084834]
        settings.FILE_UPLOAD_TEMP_DIR = '/tmp'

    def test_initiale_context(self):
        title = 'Title'
        plugin_settings = {
            'title': title
        }
        model_instance = create_model_instance(InitialeTextPlugin, **plugin_settings)
        context = get_render_context(model_instance)
        self.assertIn('title', context.keys())
        self.assertEqual(context['title'], title)

    def test_initiale_html(self):
        plugin_settings = {
            'title': 'Title'
        }
        model_instance = create_model_instance(InitialeTextPlugin, **plugin_settings)
        html = render(model_instance)
        self.assertIn('<div class="post-letter">T</div>', html)

    def test_video_html(self):
        plugin_settings = {
            'title': 'Title',
            'description': 'Description',
            'url': 'https://example.com/mov.mp4'
        }
        model_instance = create_model_instance(VideoPlugin, **plugin_settings)
        html = render(model_instance)
        self.assertIn('<iframe', html)
        self.assertIn(plugin_settings['url'], html)

    def test_fullwidth_testimonial_html(self):
        plugin_settings = {
            'name': 'Name',
            'bodytext': 'BodyText',
            'claim': 'Claim',
        }
        model_instance = create_model_instance(FullWidthTestimonialPlugin, **plugin_settings)
        html = render(model_instance)
        self.assertIn(plugin_settings['name'], html)
        self.assertIn(plugin_settings['bodytext'], html)
        self.assertIn(plugin_settings['claim'], html)

    def test_callout_html(self):
        plugin_settings = {
        }
        model_instance = create_model_instance(CalloutPlugin, **plugin_settings)
        html = render(model_instance)
        self.assertIn('<div class="callout', html)

    def test_map_contains_address_and_location_from_settings(self):
        model_instance = create_model_instance(MapPlugin)
        context = get_render_context(model_instance)
        self.assertIn('map_address', context.keys())
        self.assertIn('map_location', context.keys())
        self.assertEqual(context['map_address'], settings.MAP_ADDRESS)
        self.assertEqual(eval(context['map_location']), settings.MAP_LOCATION)

    def test_overlay_html(self):
        image_name = 'test_file.jpg'
        picture = create_gallery_image(image_name)
        plugin_settings = {
            'image': picture,
        }
        model_instance = create_model_instance(OverlayPlugin, **plugin_settings)
        model_instance.image = picture
        html = render(model_instance)
        self.assertIn(image_name, html)
