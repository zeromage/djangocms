import os
from tempfile import mkdtemp

from filer.utils.compatibility import PILImage, PILImageDraw
from filer.models.imagemodels import Image as FilerImage

from django.core.files import File
from django.test.client import RequestFactory

from cms.api import add_plugin
from cms.models import Placeholder
from cms.plugin_rendering import ContentRenderer

from grids.models import GalleryImage


def create_image(mode="RGB", size=(800, 600)):
    """
    Creates a usable image file using PIL
    :returns: PIL Image instance
    """
    image = PILImage.new(mode, size)
    draw = PILImageDraw.Draw(image)
    x_bit, y_bit = size[0] // 10, size[1] // 10
    draw.rectangle((x_bit, y_bit * 2, x_bit * 7, y_bit * 3), "red")
    draw.rectangle((x_bit * 2, y_bit, x_bit * 3, y_bit * 8), "red")

    return image


def get_image(image_name="test_file.jpg"):
    """
    Creates and stores an image to the file system using PILImage
    :param image_name: the name for the file (default "test_file.jpg")
    :returns: dict {name, image, path}
    """
    image = create_image()
    image_path = os.path.join(
        mkdtemp(),
        image_name,
    )
    image.save(image_path, "JPEG")

    return {
        "name": image_name,
        "image": image,
        "path": image_path,
    }


def get_filer_image(image_name="test_file.jpg"):
    """
    Creates and stores an image to filer and returns it
    :param image_name: the name for the file (default "test_file.jpg")
    :returns: filer image instance
    """
    image = get_image(image_name)
    filer_file = File(
        open(image.get("path"), "rb"),
        name=image.get("name"),
    )
    filer_object = FilerImage.objects.create(
        original_filename=image.get("name"),
        file=filer_file,
    )

    return filer_object


def create_gallery_image(image_name):
    filer_img = get_filer_image(image_name)
    picture = GalleryImage.objects.create(picture=filer_img)
    picture.save()
    return picture


def create_model_instance(plugin, *args, **kwargs):
    placeholder = Placeholder.objects.create(slot='test')
    model_instance = add_plugin(
        placeholder,
        plugin,
        'en',
        **kwargs,
    )
    return model_instance


def get_render_context(model_instance):
    plugin_instance = model_instance.get_plugin_class_instance()
    request = RequestFactory()
    context = plugin_instance.render({'request': request}, model_instance, None)
    return context


def render(model_instance):
    request = RequestFactory()
    renderer = ContentRenderer(request)
    html = renderer.render_plugin(model_instance, context={'request': request})
    return html
