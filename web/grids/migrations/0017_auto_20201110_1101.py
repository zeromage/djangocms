# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-11-10 11:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('grids', '0016_grid_style'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grid',
            name='custom_classes',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='custom classes'),
        ),
        migrations.AlterField(
            model_name='grid',
            name='style',
            field=models.CharField(blank=True, choices=[('', ''), ('red', 'Rot')], max_length=200, null=True, verbose_name='style'),
        ),
    ]
