# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-01-18 09:59
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import djangocms_text_ckeditor.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('djangocms_picture', '0009_auto_20181212_1003'),
        ('cms', '0020_old_tree_cleanup'),
    ]

    operations = [
        migrations.CreateModel(
            name='FullWidthTestimonialPluginModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='grids_fullwidthtestimonialpluginmodel', serialize=False, to='cms.CMSPlugin')),
                ('name', models.CharField(blank=True, max_length=70, null=True)),
                ('bodytext', djangocms_text_ckeditor.fields.HTMLField(blank=True, null=True)),
                ('claim', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='GalleryImage',
            fields=[
                ('picture_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='djangocms_picture.Picture')),
            ],
            options={
                'abstract': False,
            },
            bases=('djangocms_picture.picture',),
        ),
        migrations.CreateModel(
            name='GalleryPluginModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='grids_gallerypluginmodel', serialize=False, to='cms.CMSPlugin')),
                ('images', models.ManyToManyField(related_name='plugins', to='grids.GalleryImage')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Grid',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='grids_grid', serialize=False, to='cms.CMSPlugin')),
                ('custom_classes', models.CharField(blank=True, max_length=200, verbose_name='custom classes')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='GridColumn',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='grids_gridcolumn', serialize=False, to='cms.CMSPlugin')),
                ('size', models.CharField(choices=[('12', 'grid-12'), ('11', 'grid-11'), ('10', 'grid-10'), ('9', 'grid-9'), ('8', 'grid-8'), ('7', 'grid-7'), ('6', 'grid-6'), ('5', 'grid-5'), ('4', 'grid-4'), ('3', 'grid-3'), ('2', 'grid-2'), ('1', 'grid-1')], default='1', max_length=50, verbose_name='size')),
                ('custom_classes', models.CharField(blank=True, max_length=200, verbose_name='custom classes')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='InitialeText',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='grids_initialetext', serialize=False, to='cms.CMSPlugin')),
                ('body', models.TextField(verbose_name='body')),
                ('title', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Testimonial',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='grids_testimonial', serialize=False, to='cms.CMSPlugin')),
                ('name', models.CharField(blank=True, max_length=70, null=True)),
                ('bodytext', models.TextField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='TestimonialPluginModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='grids_testimonialpluginmodel', serialize=False, to='cms.CMSPlugin')),
                ('testimonials', models.ManyToManyField(related_name='plugins', to='grids.Testimonial')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='TimelineNode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=70, null=True)),
                ('bodytext', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TimelinePluginModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='grids_timelinepluginmodel', serialize=False, to='cms.CMSPlugin')),
                ('timeline_nodes', models.ManyToManyField(related_name='plugins', to='grids.TimelineNode')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='grids_video', serialize=False, to='cms.CMSPlugin')),
                ('title', models.CharField(blank=True, max_length=70, null=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('url', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
    ]
