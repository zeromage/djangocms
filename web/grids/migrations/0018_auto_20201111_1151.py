# Generated by Django 2.2 on 2020-11-11 11:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0022_auto_20180620_1551'),
        ('grids', '0017_auto_20201110_1101'),
    ]

    operations = [
        migrations.CreateModel(
            name='CheckmarkListPluginModel',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, related_name='grids_checkmarklistpluginmodel', serialize=False, to='cms.CMSPlugin')),
            ],
            options={
                'abstract': False,
            },
            bases=('cms.cmsplugin',),
        ),
        migrations.CreateModel(
            name='CheckmarkNode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bodytext', models.TextField(blank=True, null=True)),
                ('list_order', models.PositiveIntegerField(default=0)),
            ],
            options={
                'ordering': ['list_order'],
            },
        ),
        migrations.CreateModel(
            name='CheckmarkNodeList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('list_order', models.PositiveIntegerField(default=0)),
                ('checkmarknode', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='grids.CheckmarkNode')),
                ('plugin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='grids.CheckmarkListPluginModel')),
            ],
            options={
                'ordering': ['list_order'],
            },
        ),
        migrations.AddField(
            model_name='checkmarklistpluginmodel',
            name='list_nodes',
            field=models.ManyToManyField(related_name='plugins', through='grids.CheckmarkNodeList', to='grids.CheckmarkNode'),
        ),
    ]
