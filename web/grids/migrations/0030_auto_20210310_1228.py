# Generated by Django 2.2 on 2021-03-10 12:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('grids', '0029_auto_20210310_1224'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gridcell',
            name='large_size',
            field=models.CharField(blank=True, choices=[('12', 'grid-12'), ('11', 'grid-11'), ('10', 'grid-10'), ('9', 'grid-9'), ('8', 'grid-8'), ('7', 'grid-7'), ('6', 'grid-6'), ('5', 'grid-5'), ('4', 'grid-4'), ('3', 'grid-3'), ('2', 'grid-2'), ('1', 'grid-1'), ('', 'No size')], default='1', max_length=50, null=True, verbose_name='Large size'),
        ),
        migrations.AlterField(
            model_name='gridcell',
            name='medium_size',
            field=models.CharField(blank=True, choices=[('12', 'grid-12'), ('11', 'grid-11'), ('10', 'grid-10'), ('9', 'grid-9'), ('8', 'grid-8'), ('7', 'grid-7'), ('6', 'grid-6'), ('5', 'grid-5'), ('4', 'grid-4'), ('3', 'grid-3'), ('2', 'grid-2'), ('1', 'grid-1'), ('', 'No size')], default='1', max_length=50, null=True, verbose_name='Medium size'),
        ),
        migrations.AlterField(
            model_name='gridcell',
            name='small_size',
            field=models.CharField(blank=True, choices=[('12', 'grid-12'), ('11', 'grid-11'), ('10', 'grid-10'), ('9', 'grid-9'), ('8', 'grid-8'), ('7', 'grid-7'), ('6', 'grid-6'), ('5', 'grid-5'), ('4', 'grid-4'), ('3', 'grid-3'), ('2', 'grid-2'), ('1', 'grid-1'), ('', 'No size')], default='1', max_length=50, null=True, verbose_name='Small size'),
        ),
    ]
