# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext_lazy as _

from captcha.fields import CaptchaField

from .models import Grid, GRID_CONFIG, DJANGOCMS_GRID_CHOICES, ContactFormEntry


NUM_COLUMNS = [
    (i, '%s' % i) for i in range(0, GRID_CONFIG['COLUMNS'])
]


class GridPluginForm(forms.ModelForm):
    """Form for the grid plugin

    The form presents the relevant fields for the
    settings for a grid plugin.
    """
    create = forms.ChoiceField(
        choices=NUM_COLUMNS, label=_('Create Columns'),
        initial='1',
        help_text=_('Create this number of columns inside'))
    create_size = forms.ChoiceField(
        choices=DJANGOCMS_GRID_CHOICES, label=_('Column size'),
        required=False,
        help_text=('Width of created columns. You can still change the width '
                   'of the column afterwards.'))

    class Meta:
        model = Grid
        # fields = ['create', 'create_size', 'style', 'custom_classes']
        fields = ['create', 'create_size', 'grid_type', 'margin_x', 'margin_y', 'padding_x', 'padding_y',
                  'frame', 'small_frame', 'medium_frame', 'large_frame',
                  'padding_gutters', 'grid_container', 'fluid', 'light',
                  'style', 'custom_classes', 'image', 'content_element']
        exclude = ('page', 'position', 'placeholder', 'language', 'plugin_type')


class GridDoublePluginForm(forms.ModelForm):
    """Form for the double grid plugin (two columns/cells)

    The form presents the relevant fields for the
    settings for a double grid plugin.
    """

    class Meta:
        model = Grid
        fields = ['margin_x', 'margin_y', 'padding_x', 'padding_y',
                  'frame', 'small_frame', 'medium_frame', 'large_frame',
                  'padding_gutters', 'grid_container', 'fluid', 'light',
                  'style', 'custom_classes', 'image', 'content_element']
        exclude = ('page', 'position', 'placeholder', 'language', 'plugin_type')


class GridBasePluginForm(forms.ModelForm):

    class Meta:
        model = Grid
        exclude = ('page', 'position', 'placeholder', 'language', 'plugin_type')


class ContactForm(forms.ModelForm):
    """Form for the contact form plugin

    The form presents the relevant fields for the
    settings for a contact form plugin.
    """
    captcha = CaptchaField()

    class Meta:
        model = ContactFormEntry
        exclude = []
