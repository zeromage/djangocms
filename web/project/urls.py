# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve
# from django.urls import path
from django.conf.urls import include, url

admin.autodiscover()

urlpatterns = [
    url(r'^sitemap\.xml$', sitemap,
        {'sitemaps': {'cmspages': CMSSitemap}}),
]

# urlpatterns += i18n_patterns(
urlpatterns += [
    url(r'^captcha/', include('captcha.urls')),
    url(r'^admin/', admin.site.urls),  # NOQA
    url(r'^xfiler/', include('filer.urls')),
    url(r'^', include('cms.urls')),
    url(r'^contact/', include('contact_form.urls')),
]

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = [
        url(r'^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        #path('__debug__/', include(debug_toolbar.urls)),
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
