from django.conf import settings # import the settings file

def project_context(request):
    return {'project_id': settings.PROJECT_ID}
