from pipeline.compressors import CompressorBase
from csscompressor import compress

class CssCompressor(CompressorBase):
  def compress_css(self, js):
      return compress(js)
