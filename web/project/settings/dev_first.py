from .base import *

DEBUG = True

SITE_ID = 1

ALLOWED_HOSTS = ['*']

CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        }
}

PROJECT_ID = 'first'

PROTOCOL = 'http'
