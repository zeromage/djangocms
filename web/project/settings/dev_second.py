from .base import *

DEBUG = True

SITE_ID = 2

ALLOWED_HOSTS = ['*']

PROJECT_ID = 'second'

CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        }
}

PROTOCOL = 'http'

MAP_ADDRESS = '<b>Second Company</b><br>Some street<br>55555 City'
MAP_LOCATION = [50.7985410, 7.2084834]
