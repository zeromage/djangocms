#!/usr/bin/env bash

docker inspect --format '{{ .NetworkSettings.Networks.jobdocker.IPAddress }}' "$1"
