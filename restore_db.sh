export PROJECT=${PWD##*/} 
echo "$PROJECT"
podman cp --pause=false /home/docka/backups/"$PROJECT"_dump_$(date +%F).sql docka_db_1:/var/lib/postgresql
podman stop "$PROJECT"_web_1
podman stop "$PROJECT"docka_test_1
podman stop "$PROJECT"docka_worker_1
podman exec -u postgres -ti docka_db_1 sh -c "dropdb docka"
podman exec -u postgres -ti docka_db_1 sh -c "createdb docka --owner docka"
podman exec -u postgres -ti docka_db_1 sh -c "psql docka < /var/lib/postgresql/"$PROJECT"_dump_$(date +%F).sql"
