FROM registry.gitlab.com/zeromage/djangocmsbase:3.8.0

ADD build/nginx /etc/logrotate.d/nginx
ADD build/uwsgi /etc/logrotate.d/uwsgi

ENV LANG de_DE.UTF-8  
ENV LANGUAGE de_DE:en  
ENV LC_ALL de_DE.UTF-8 

WORKDIR /usr/src/app
COPY web/requirements.txt /usr/src/app/
RUN pip3 install -r requirements.txt

RUN export PS1='$(whoami)@$(hostname):$(pwd) '

#RUN apk add sassc
WORKDIR /
USER root
# install sass
#USER root
#RUN apk --update add git build-base
#RUN git clone https://github.com/sass/sassc
#WORKDIR /sassc
#RUN git clone https://github.com/sass/libsass
#RUN SASS_LIBSASS_PATH=/sassc/libsass make
## install
#RUN mv bin/sassc /usr/bin/sassc
## cleanup
#WORKDIR /
#RUN rm -rf /sassc

WORKDIR /

RUN apk del git build-base
RUN apk add libstdc++ # sass binary still needs this because of dynamic linking.
RUN rm -rf /var/cache/apk/*
WORKDIR /usr/src/app

# run entrypoint.sh
ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
