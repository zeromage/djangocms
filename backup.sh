export PROJECT=${PWD##*/} 
echo "$PROJECT"
podman exec -ti --user postgres "$PROJECT"_db_1 bash -c 'pg_dump -U postgres djangocms' > /home/docka/backups/"$PROJECT"_dump_$(date +%F).sql
gzip -f /home/docka/backups/"$PROJECT"_dump_$(date +%F).sql
tar -cf /home/docka/backups/"$PROJECT"_files_$(date +%F) .
gzip -f /home/docka/backups/"$PROJECT"_files_$(date +%F)
